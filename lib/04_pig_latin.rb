VOWELS = %w[a e i o u].freeze


def translate(string)
  words = string.split(' ')
  translated_words = translate_array(words)
  translated_words.join(' ')
end

private

def translate_array(array)
  array.map do |word|
    translate_word(word)
  end
end

def translate_word(word)

  if include_vowel?(word[0])
    vowel_translate(word)
  else
    consonant_translate(word)
  end
end

def include_vowel?(letter)
  VOWELS.include?(letter)
end

def vowel_translate(word)
  word + 'ay'
end

def consonant_translate(word)
  if three_consonants_start?(word)
    three_cons_translate(word)
  elsif two_consonants_start?(word)
    contains_qu?(word) ? three_cons_translate(word) : two_cons_translate(word)
  else
    contains_qu?(word) ? two_cons_translate(word) : one_cons_translate(word)
  end
end

def contains_qu?(word)
  (word[0] == 'q' && word[1] == 'u') || (word[1] == 'q' && word[2] == 'u')
end

def one_cons_translate(word)
  word[1..-1] + word[0] + 'ay'
end

def two_cons_translate(word)
  word[2..-1] + word[0..1] + 'ay'
end

def three_cons_translate(word)
  word[3..-1] + word[0..2] + 'ay'
end

def two_consonants_start?(word)
  include_vowel?(word[0]) == false && include_vowel?(word[1]) == false
end

def three_consonants_start?(word)
  two_consonants_start?(word) == true && include_vowel?(word[2]) == false
end
