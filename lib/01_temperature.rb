def ftoc(c_input)
  (c_input-32)*5/9.0
end

def ctof(f_input)
  (f_input*9/5.0)+32
end
