def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string,repeat=2)
  repeated_string=string
  number_of=repeat-1
  number_of.times do
    repeated_string+= " #{string}"
  end
  repeated_string
end

def start_of_word(string,times)
  string[0..times-1]
end

def first_word(string)
  string[0...string.index(' ')]
end

def titleize(string)
  little_words=["and","the","over"]
  words=string.split(' ')
  words[0].capitalize!
  words[1..-1]. each do |w|
    w.capitalize! unless little_words.include?(w)
  end
  words.join(' ')
end
